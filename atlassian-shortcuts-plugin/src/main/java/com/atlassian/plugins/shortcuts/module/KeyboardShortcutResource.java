package com.atlassian.plugins.shortcuts.module;

import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutManager;
import com.atlassian.sal.api.message.I18nResolver;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Provides access to the keyboard shortcuts currently registered in the plugin system.
 *
 * @since v4.1
 */
@Path ("/")
@AnonymousAllowed
public class KeyboardShortcutResource
{
    private final JaxbJsonMarshaller jaxbJsonMarshaller;
    private static final CacheControl NO_CACHE = new CacheControl();
    static
    {
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }

    private static final CacheControl CACHE_FOREVER = new CacheControl();
    static
    {
        CACHE_FOREVER.setPrivate(false);
        CACHE_FOREVER.setMaxAge(Integer.MAX_VALUE);
    }
    private final KeyboardShortcutManager keyboardShortcutManager;

    private final I18nResolver i18nResolver;

    public KeyboardShortcutResource(final KeyboardShortcutManager keyboardShortcutManager, final I18nResolver i18nResolver)
    {
        this.keyboardShortcutManager = keyboardShortcutManager;
        this.jaxbJsonMarshaller = new DefaultJaxbJsonMarshaller();
        this.i18nResolver = i18nResolver;
    }

    /**
     * Returns a JSON representation of all the keyboard shortcuts currently registered.
     *
     * @return a JSON representation of all the keyboard shortcuts currently registered.
     */
    /*
       Include the buildnumber as well as the hash of the keyboard shortcuts,
       to ensure that when the app is upgraded new keyboard shortcuts will be
       served regardless of whether the hashcode has changed or not.
       Using this requires extra integration with page load, but allows the resource to be cached indefinitely.
     */
    @GET
    @Path("shortcuts/{buildnumber}/{hashcode}")
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getShortCuts()
    {
        final List<KeyboardShortcut> shortcuts = keyboardShortcutManager.getAllShortcuts();
        try
        {
            String jsonString = jaxbJsonMarshaller.marshal(new Shortcuts(shortcuts, i18nResolver), Shortcuts.class, Shortcut.class);
            if("null".equals(jsonString))
            {
                jsonString = "{}";
            }
            return Response.ok(jsonString).cacheControl(CACHE_FOREVER).build();
        }
        catch (JAXBException e)
        {
            return Response.noContent().cacheControl(NO_CACHE).build();
        }
    }

    @XmlRootElement
    public static class Shortcuts
    {
        @XmlElement
        final List<Shortcut> shortcuts = new ArrayList<Shortcut>();

        private Shortcuts() {}

        public Shortcuts(List<KeyboardShortcut> origShortcuts, I18nResolver i18nResolver)
        {
            for (KeyboardShortcut origShortcut : origShortcuts)
            {
                shortcuts.add(new Shortcut(origShortcut, i18nResolver));
            }
        }
    }

    @XmlRootElement
    public static class Shortcut
    {
        @XmlElement
        private Set<List<String>> keys;
        @XmlElement
        private String context;
        @XmlElement
        private String op;
        @XmlElement
        private String param;
        @XmlElement
        private String descKey;
        @XmlElement
        private String description;
        @XmlElement
        private Boolean hidden;

        private Shortcut() {}

        public Shortcut(KeyboardShortcut shortcut, I18nResolver i18nResolver)
        {
            this.keys = new LinkedHashSet<List<String>>(shortcut.getShortcuts());
            this.context = shortcut.getContext();
            this.op = shortcut.getOperation().getType().name();
            this.param = shortcut.getParameter();
            this.descKey = shortcut.getDescriptionI18nKey();

            String description = i18nResolver.getText(this.descKey);
            if (description == null || description.equals(this.descKey)) {
                description = shortcut.getDefaultDescription();
            }
            this.description = description;

            this.hidden = shortcut.isHidden();
        }
    }
}
