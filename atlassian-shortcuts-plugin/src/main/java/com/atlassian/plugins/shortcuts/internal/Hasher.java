package com.atlassian.plugins.shortcuts.internal;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Uses an MD5 or SHA {@link MessageDigest} to turn an object into a hash
 * string.
 */
public class Hasher
{
    public static String getHash(Object shortcuts)
    {
        MessageDigest messageDigest = getMessageDigest();
        messageDigest.update(shortcuts.toString().getBytes());
        final byte[] digest = messageDigest.digest();
        final BigInteger bigInt = new BigInteger(1, digest);
        final String hash = bigInt.toString(16);
        return hash;
    }

    private static MessageDigest getMessageDigest()
    {
        MessageDigest messageDigest = getMessageDigest("MD5");
        if (messageDigest == null)
        {
            messageDigest = getMessageDigest("SHA");
        }
        if (messageDigest == null)
        {
            throw new RuntimeException( "Unable to retrieve a valid message digest!");
        }
        return messageDigest;
    }

    private static MessageDigest getMessageDigest(String digestName)
    {
        try
        {
            return MessageDigest.getInstance(digestName);
        }
        catch (NoSuchAlgorithmException e)
        {
            return null;
        }
    }
}