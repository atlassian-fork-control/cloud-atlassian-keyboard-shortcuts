/**
 * @fileoverview
 * We use a number of events to communicate with the JavaScript provided by the host application to allow
 * us to enable and disable keyboard shortcuts.
 *
 * initialize.keyboardshortcuts - The host application should trigger this event when the page is ready
 * to bind the keyboard shortcuts. The host application must ensure that any necessary internationalization
 * resources are available before triggering this event.
 *
 * register-contexts.keyboardshortcuts - keycommands.js will trigger this event and pass the shortcutsRegistry
 * object to the bound function. The host application should bind a function to this event which will take the
 * shortcutsRegistry object and call enableContext: for each context fow which it wishes to enable shortcuts e.g.
 *
 * AJS.bind("register-contexts.keyboardshortcuts", function (e, data) {
 *      data.shortcutRegistry.enableContext("viewcontent");
 * });
 *
 * remove-bindings.keyboardshortcuts - The host application can trigger this event to disable all keyboard
 * shortcuts.
 *
 * add-bindings.keyboardshortcuts - The host application can trigger this event to re-enable all keyboard
 * shortcuts if 'remove-bindings.keyboardshortcuts' was triggered previously.
 *
 *
 *
 * Example product code:
 *
 * AJS.bind("register-contexts.keyboardshortcuts", function(e, data) {
 *     data.shortcutRegistry.enableContext("global");
 *     data.shortcutRegistry.enableContext("my-context");
 * });
 *
 * AJS.trigger("initialize.keyboardshortcuts");
 *
 */
AJS.bind("initialize.keyboardshortcuts", function () {

    var $ = AJS.$,
        get = function(key) {
            return (AJS.Data && AJS.Data.get(key)) ||
                   AJS.params[key];
        },
        buildNumber = get("build-number"),
        hash = get("keyboardshortcut-hash");

    if (!buildNumber || !hash) {
        throw new Error("build-number and keyboardshortcut-hash must both exist in AJS.Data or AJS.params.");
    }

    var shortcutPath = AJS.contextPath() + "/rest/shortcuts/latest/shortcuts/" + encodeURIComponent(buildNumber) + '/' + encodeURIComponent(hash);

    $.getJSON(shortcutPath, function(json) {
        var shortcuts = json.shortcuts;

        if (!shortcuts) throw new Error ("Server returned no shortcuts.");
        AJS.trigger("shortcuts-loaded.keyboardshortcuts", {shortcuts: shortcuts});

        /**
         * The shortcutRegistry object allows the host application to enable shortcuts for a given context.
         */

        var enabledShortcuts = [];

        var shortcutRegistry = {
            /**
             *
             * @param {String} context. All shortcuts with the given context will be enabled.
             */

            enableContext: function (context) {
                var contextShortcuts = $.grep(shortcuts, function(shortcut) {
                    return shortcut.context === context;
                });
                enabledShortcuts = enabledShortcuts.concat(AJS.whenIType.fromJSON(contextShortcuts, true));
            }
        };

        var addKeyBindings = function() {
            AJS.trigger("register-contexts.keyboardshortcuts", {shortcutRegistry: shortcutRegistry});
        };
        addKeyBindings();
        AJS.bind("add-bindings.keyboardshortcuts", addKeyBindings);

        var removeKeyBindings = function() {
            $.each(enabledShortcuts, function() { this.unbind(); });
            enabledShortcuts = [];
        };
        AJS.bind("remove-bindings.keyboardshortcuts", removeKeyBindings);
    });
});

