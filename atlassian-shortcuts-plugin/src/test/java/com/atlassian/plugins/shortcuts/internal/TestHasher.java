package com.atlassian.plugins.shortcuts.internal;

import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;

public class TestHasher extends TestCase
{
    public void testHash() throws Exception
    {
        Object obj = new Object();

        String hash1 = Hasher.getHash(obj);
        assertTrue(StringUtils.isNotBlank(hash1));

        String hash2 = Hasher.getHash(obj);
        assertEquals(hash1, hash2);
    }
}
