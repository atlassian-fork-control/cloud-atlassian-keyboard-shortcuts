package com.atlassian.plugins.shortcuts.api;

import com.atlassian.plugin.util.Assertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Defines a keyboard shortcut. A shortcut consists of the keys required for the shortcut (could be multiples for the
 * same shortcut such as gh or gd), a context (some shortcuts only apply on the view issue page), the operation and a
 * parameter for the operation (generally a jQuery selector, but can be anything really.).
 */
public final class KeyboardShortcut implements Comparable<KeyboardShortcut>
{
    private final Set<List<String>> shortcuts;
    private final String context;
    private final KeyboardShortcutOperation operation;
    private final String descriptionI18nKey;
    private final String defaultDescription;
    private final int order;
    private final boolean hidden;

    public KeyboardShortcut(final String context, final KeyboardShortcutOperation operation, final int order, final Set<List<String>> shortcuts, final String descriptionI18nKey, final String defaultDescription, final boolean hidden)
    {
        this.hidden = hidden;
        this.context = Assertions.notNull("context", context);
        this.shortcuts = new LinkedHashSet<List<String>>(shortcuts);
        this.operation = Assertions.notNull("operation", operation);
        this.order = order;
        this.descriptionI18nKey = descriptionI18nKey;
        this.defaultDescription = defaultDescription;
    }

    public String getContext()
    {
        return context;
    }

    public Set<List<String>> getShortcuts()
    {
        return shortcuts;
    }

    public int getOrder()
    {
        return order;
    }

    public KeyboardShortcutOperation getOperation()
    {
        return operation;
    }

    public String getParameter()
    {
        return operation.getParam();
    }

    public String getDescriptionI18nKey()
    {
        return descriptionI18nKey;
    }

    public String getDefaultDescription()
    {
        return defaultDescription;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public int compareTo(final KeyboardShortcut shortcut)
    {
        final int order2 = shortcut.getOrder();

        if (order == order2)
        {
            return 0;
        }
        else if (order < order2)
        {
            return -1;
        }
        return 1;
    }

    @Override
    public String toString()
    {
        return "KeyboardShortcut{" +
               "context=" + context +
               ", shortcuts=" + shortcuts +
               ", operation=" + operation +
               ", parameter='" + operation.getParam() + '\'' +
               ", descriptionI18nKey='" + descriptionI18nKey + '\'' +
               ", description='" + defaultDescription + '\'' +
               ", order=" + order +
               ", hidden=" + hidden +'}';
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof KeyboardShortcut)
        {
            KeyboardShortcut otherShortcut = (KeyboardShortcut) other;
            if(this.operation.equals(otherShortcut.getOperation()) &&
                    this.shortcuts.equals(otherShortcut.getShortcuts()) &&
                    this.context.equals(otherShortcut.getContext()))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return (41 * (31 + this.operation.hashCode()) + 29 * this.shortcuts.hashCode() + this.context.hashCode());
    }

    private Set<List<String>> copyShortcuts()
    {
        Set<List<String>> result = new LinkedHashSet<List<String>>();
        for(List<String> shortcut : this.shortcuts)
        {
            result.add(new ArrayList<String>(shortcut));
        }
        return result;
    }

    public static Builder builder(KeyboardShortcut copyFrom)
    {
        return new Builder(copyFrom);
    }

    public static class Builder
    {
        private Set<List<String>> shortcuts;
        private String context;
        private KeyboardShortcutOperation operation;
        private String descriptionI18nKey;
        private String defaultDescription;
        private int order;
        private boolean hidden;

        public Builder(KeyboardShortcut copyFrom)
        {
            this.context = copyFrom.getContext();
            this.operation = new KeyboardShortcutOperation(copyFrom.getOperation().getType().name(), copyFrom.getOperation().getParam());
            this.order = copyFrom.getOrder();
            this.shortcuts = copyFrom.copyShortcuts();
            this.descriptionI18nKey = copyFrom.getDescriptionI18nKey();
            this.defaultDescription = copyFrom.getDefaultDescription();
            this.hidden = copyFrom.isHidden();
        }

        public void setHidden(boolean hidden)
        {
            this.hidden = hidden;
        }

        public void setOrder(int order)
        {
            this.order = order;
        }

        public void setDefaultDescription(String defaultDescription)
        {
            this.defaultDescription = defaultDescription;
        }

        public void setDescriptionI18nKey(String descriptionI18nKey)
        {
            this.descriptionI18nKey = descriptionI18nKey;
        }

        public void setContext(String context)
        {
            this.context = context;
        }

        public void setShortcuts(Set<List<String>> shortcuts)
        {
            this.shortcuts = shortcuts;
        }

        public void setOperationType(String operationType)
        {
            this.operation = new KeyboardShortcutOperation(operationType, this.operation.getParam());
        }

        public void setOperationParam(String param)
        {
            this.operation = new KeyboardShortcutOperation(this.operation.getType().name(), param);
        }

        public KeyboardShortcut build()
        {
            return new KeyboardShortcut(
                this.context,
                this.operation,
                this.order,
                this.shortcuts,
                this.descriptionI18nKey,
                this.defaultDescription,
                this.hidden
            );
        }
    }
}
